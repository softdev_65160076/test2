
package com.mycompany.testlab1;


import java.util.Scanner;
// Can play but rematch don't work 
/**
 *
 * @author ASUS
 */
public class Testlab1 {
    public static int i ;
    public static int j ;
    public static int row ;
    public static int col ;
    public String X = "X";
    public String O = "O";
    static String[][] a = new String[3][3];
    static boolean P = true ;
    static String re = null;
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        
        System.out.println("Welcome To XO Game!!!");
        System.out.println("X Always Start First!");
        
        for(i = 0; i < 3; i++){
            for(j = 0; j < 3; j++){
                a[i][j] = "-";
                System.out.print(a[i][j]+" ");
            }
        System.out.println();
        }
        
        while(P == true){
            for(i = 1; i < 10; i++){
                if(i < 10){
                    System.out.println("X Turn");
                    System.out.println("Please Input Row & Colum: ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    changeX(row, col);
                    if(CheckWin() == true){
                        break;
                    }
                    System.out.println("O Turn");
                    System.out.println("Please Input Row & Colum: ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    changeO(row, col);
                    if(CheckWin()==true){
                        break;
                    }
                }else{
                    Draw();
                }
            }
            System.out.println("Game End!!!");
            System.out.println("Rematch ?");
            re = kb.next();
            P = rematch(re);
        }
            System.out.println("Thank You For Play");
    }
    
    public static boolean CheckWin(){
        //X Win
        if ((a[0][0] == "X" && a[0][1] == "X" && a[0][2] == "X") ||
            (a[1][0] == "X" && a[1][1] == "X" && a[1][2] == "X") ||
            (a[2][0] == "X" && a[2][1] == "X" && a[2][2] == "X") ||
            (a[0][0] == "X" && a[1][0] == "X" && a[2][0] == "X") ||
            (a[0][1] == "X" && a[1][1] == "X" && a[2][1] == "X") ||
            (a[0][2] == "X" && a[1][2] == "X" && a[2][2] == "X") ||
            (a[0][0] == "X" && a[1][1] == "X" && a[2][2] == "X") ||
            (a[2][0] == "X" && a[1][1] == "X" && a[0][2] == "X")) {
        System.out.println("The Winner is X!!!");
            return true;
        } 
        //O Win
        else if ((a[0][0] == "O" && a[0][1] == "O" && a[0][2] == "O") ||
           (a[1][0] == "O" && a[1][1] == "O" && a[1][2] == "O") ||
           (a[2][0] == "O" && a[2][1] == "O" && a[2][2] == "O") ||
           (a[0][0] == "O" && a[1][0] == "O" && a[2][0] == "O") ||
           (a[0][1] == "O" && a[1][1] == "O" && a[2][1] == "O") ||
           (a[0][2] == "O" && a[1][2] == "O" && a[2][2] == "O") ||
           (a[0][0] == "O" && a[1][1] == "O" && a[2][2] == "O") ||
           (a[2][0] == "O" && a[1][1] == "O" && a[0][2] == "O")) {
        System.out.println("The Winner is O!!!");
            return true;
        }
        return false;
    }
    
    public static void Draw(){
        System.out.println("Draw!!!");
    }
    
    public static void changeX(int row,int col){
        if (row >= 1 && row <= 3 && col >= 1 && col <= 3) {
            a[row - 1][col - 1] = "X";
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }   
    
    public static void changeO(int row,int col){
        if (row >= 1 && row <= 3 && col >= 1 && col <= 3) {
            a[row - 1][col - 1] = "O";
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    public static boolean rematch(String re){
        if(re == "Y"){
            return true;
        }else{
            return false;
        }
    }
}

